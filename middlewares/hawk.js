/*global require: true, module:true, console: true */
/*jslint nomen: true*/

var hawk = require('hawk'),
    common = require('./common');

// TODO - change from memory cache to redis server for distribution.
var memoryCache = {};

var nonceFunc = function (nonce, ts, callback) {
    'use strict';
    if (memoryCache[nonce]) {
        return callback(new Error());
    }

    memoryCache[nonce] = true;
    return callback();
};

// Obtain Project Secret for a give Project id
var credentialsFunc = function (id, callback) {
    'use strict';
    
    // TODO - must obtain credentials from mongodb
    if (id === '1') {
        var credentials = {
            id: id,
            algorithm: 'sha256',
            key: '2'
        };
        return callback(null, credentials);
    }
	
    return callback({msg: 'invalid credentials', status: 401});
};

// Authentication response
var authenticate = function (options, req, res, next) {
    'use strict';
    
    hawk.server.authenticate(req, credentialsFunc, options, function (err, credentials, artifacts) {
        
        req.auth = {
            credentials: credentials,
            artifacts: artifacts
        };
            
        if (!err) {
            // Generate Server-Authorization response header
            // header = hawk.server.header(credentials, artifacts, { payload: '', contentType: 'application/json' });
            // res.header('Server-Authorization', header);

            req.api = {id: credentials.id, key: credentials.key};
            
            return next();
        }
        
		console.log(err);
    
		console.log('\nStatus: ' + 401);
        // sends a unauthorized request.
        res.status(401).end();
		console.log('\nSent');
    });
};

var defaultRequestOptions = function (req, res) {
    'use strict';
	console.log(JSON.stringify(req.body));
	console.log('HAS BODY? ' + Object.keys(req.body).length);
	if (Object.keys(req.body).length) {
		return {
			nonceFunc: nonceFunc,
			payload: JSON.stringify(req.body)
		};
	} else {
		return {
			nonceFunc: nonceFunc
		};
	}
};

var middleware = function (options) {
    'use strict';
    options = options || defaultRequestOptions;
    if (typeof (options) !== 'function') {
        options = defaultRequestOptions;
    }
    return function (req, res, next) {
        authenticate(options(req, res), req, res, next);
    };
};

module.exports = middleware();