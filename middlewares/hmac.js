/*global require: true, module:true */
/*jslint nomen: true*/

var Hmmac = require('hmmac'),
    mongoose = require('mongoose');

var project = mongoose.model('Project');

var credProvider = function (key, callback) {
    'use strict';
    
    console.log('Request for key: ' + key);
    
    if (key === '1') {
        console.log('callback');
        callback({
            key: '1',
            secret: '1'
        });
    } else {
        callback(null);
    /* 
    var query = project.where({id: key});
    query.findOne(function (err, project) {
        
        if (!project) {
            throw 'Invalid project_id or project_secret.';
        }
        
        console.log('Request for key: ' + key + ' - ' + project.project_secret);
        
        callback({
            key: project.project_id,
            secret: project.project_secret
        });
        
    });*/
    }
};

var hmmac = new Hmmac({
    credentialProvider: credProvider,
    debug: 2,
    signedHeaders: [ 'host', 'content-type', 'date' ]
}), customResponder = function (valid, req, res, next) {
    'use strict';
    if (valid) {
        return next();
    }
    res.send(401);
};

module.exports = Hmmac.middleware(hmmac, customResponder);