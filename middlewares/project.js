/*global module: true, require: true*/
/*jslint nomen:true */

var mongoose = require('mongoose'),
    common = require('./common'),
    project = mongoose.model('Project');

var projectMiddlewares = {
    addOrUpdate: function (req, res, next) {
        'use strict';
        
        var projectId = req.body.id,
            key = req.body.key;
		
		console.log('key: ' + key);
		console.log('projectId: ' + projectId);
		
		if (!projectId || !key) {
			return res.status(400).send();
		}
		
        common.resolve(project.addOrUpdate(projectId, key), req, next);
    },
	remove: function (req, res, next) {
		'use strict';
		
		var projectId = req.params.projectId;
		
		if (!projectId) {
			return res.status(400).send();
		}
		
		common.resolve(project.remove(projectId), req, next);
		
	},
    get: function (req, res, next) {
        'use strict';
        
        var projectId = req.params.projectId;
        		
		if (!projectId) {
			return res.status(400).send();
		}
		
        common.resolve(project.get(projectId), req, next);
        
    }
};

module.exports = projectMiddlewares;