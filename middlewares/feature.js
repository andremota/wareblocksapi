/*global module: true, require: true*/

var mongoose = require('mongoose'),
    common = require('./common'),
    feature = mongoose.model('Feature');



var features = {
    add: function (req, res, next) {
        'use strict';
        
        var projectId = req.params.projectId,
            permission = req.body.permission,
			tokens = req.body.tokens;
		
		if (!projectId || !permission) {
			return res.status(400).send();
		}
		
        common.resolve(feature.add(projectId, permission, tokens || []), req, next);
    },
	addOrUpdate: function (req, res, next) {
        'use strict';
        
        var projectId = req.params.projectId,
            featureId = req.params.featureId,
			tokens = req.body.tokens;
        
		if (!projectId || !featureId) {
			return res.status(400).send();
		}
		
        common.resolve(feature.addOrUpdate(projectId, featureId, tokens), req, next);
    },
    get: function (req, res, next) {
        'use strict';
        
        var projectId = req.params.projectId,
            featureId = req.params.featureId;
		
        if (!projectId || !featureId) {
			return res.status(400).send();
		}
		
        common.resolve(feature.get(projectId, featureId), req, next);
    },
	remove: function (req, res, next) {
		'use strict';
		
		var projectId = req.params.projectId,
			featureId = req.params.featureId;
		
		if (!projectId || !featureId) {
			return res.status(400).send();
		}
		
		common.resolve(feature.remove(projectId, featureId), req, next);
		
	}
};

module.exports = features;