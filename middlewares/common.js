/*global require: true, module: true */
var nconf = require('nconf'),
    hawk = require('hawk');

/*
 * This file contains common middlewares.
 * These middlewares are used to help to build the api.
 * May be used in any route.
 */

var middlewares = {};
    
middlewares.resolve = function (promise, req, next) {
    // resolves promise
    'use strict';
    promise
        .then(function (model) {
			console.log(model);
            req.model = model;
        })
        .fail(function (err) {
			console.log(err);
            req.err = err;
        }).fin(function () {
            next();
        });
};

middlewares.serverAuth = function (req, res, payload) {
    'use strict';
	payload = payload ? JSON.stringify(payload) : '';
    var header = hawk.server.header(req.auth.credentials, req.auth.artifacts, {payload: payload, contentType: 'application/json'});
    res.setHeader('server-authorization', header);
    res.setHeader('content-type', 'application/json');
};

middlewares.params = function (req, err, model) {
    // loads the params needed for others middlewares.
    'use strict';
    req.err = err;
    req.model = model;
};

middlewares.validateApi = function (req, res, next) {
    // validates if the header api id and params.projectId is the same
    'use strict';
    if (req.api && req.api.id === req.params.projectId) {
        return next();
    }
    middlewares.serverAuth(req, res);
    res.status(403).end();
};

middlewares.errorResponse = function (req, res, next) {
    // validates if there was an error
    'use strict';
        
    var err = req.err;
        
    if (!err || req.model) {
        return next();
	}
        
    middlewares.serverAuth(req, res);
    
    res.status(500).end();
};
    
middlewares.modelResponse = function (req, res) {
    // sends the model back to the client.
    'use strict';
    if (!req.model) {
		middlewares.serverAuth(req, res);
        res.status(404).send('');
        return;
	}
	console.log(req.model);
    middlewares.serverAuth(req, res, req.model);
    res.send(req.model);
};
    
middlewares.noContentResponse = function (req, res) {
    // 204 response 
    'use strict';
	middlewares.serverAuth(req, res);
    res.status(204).end();
};

middlewares.wareblock = function (req, res, next) {
    // restrict usage to wareblock application id
    'use strict';
    var wareblocksId = nconf.get('wareblocks_app_id');
        
    if (req.api.id === wareblocksId) {
        return next();
    }
    
	middlewares.serverAuth(req, res);
        
    res.status(401).end();
};

module.exports = middlewares;