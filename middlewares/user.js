/*global module: true, require: true*/
/*jslint nomen:true */

var mongoose = require('mongoose'),
    common = require('./common'),
    user = mongoose.model('User');

var userMiddlewares = {
    addPermissions: function (req, res, next) {
        'use strict';
        
        var permissions = req.params.permissions,
            userId = req.params.userId,
            projectId = req.api.id || req.params.projectId;
		
		if (!projectId || !userId) {
			return res.status(400).send();
		}
        
        common.resolve(user.addOrUpdate(projectId, userId, permissions), req, next);
        
    },
    getPermissions: function (req, res, next) {
        'use strict';
        
        var userId = req.params.userId,
            projectId = req.api.id;
        
		if (!projectId || !userId) {
			return res.status(400).send();
		}
	 	
		common.resolve(user.get(projectId, userId), req, next);
		
    }
};

module.exports = userMiddlewares;