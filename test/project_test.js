/*global require: true, exports: true */
var mock = require('./hawkMock'),
    mockMessage = require('./mockMessage.js')(mock),
    invalidCredentials = mock.credentials('1', '5');

exports.project_tests = {
    // test set up
    setUp: function (done) {
        'use strict';
        done();
    },
	// a request that is not signed must be unauthorized
	'project creation wihout credentials should not be possible': function (test) {
        'use strict';
        
        var message = mockMessage.projects.create('to create', 'to create');
        
        mock.request(message)
			.then(function (result) {
				test.equal(result.response.statusCode, 401, 'Status code must be 401.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
		
    },
    // an application which isn't wareblocks should not be able to create a project.
    'project creation with invalid credentials should not be possible': function (test) {
        'use strict';
    
        var message = mockMessage.projects.create('to create', 'to create');
		
		mock.request(message, invalidCredentials)
			.then(function (result) {
				test.equal(result.response.statusCode, 401, 'Status code must be 401.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
		
    },
    // an application which isn't wareblocks shoulnd't be able to get project information
    getting_a_project_with_invalid_credentials_should_not_be_possible: function (test) {
        'use strict';
    
        var message = mockMessage.projects.get('2');
		
        mock.request(message, invalidCredentials)
			.then(function (result) {
				test.equal(result.response.statusCode, 401, 'Status code must be 401.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
		
    },
    // a request to get project info that is not signed should be rejected
    'getting a project without credentials should not be possible': function (test) {
        'use strict';
        
        // get a random project that does not exist
        var message = mockMessage.projects.get('2');
		
		
        mock.request(message)
			.then(function (result) {
				test.equal(result.response.statusCode, 401, 'Status code must be 401.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
        

    },
   // wareblocks application should be able to get a project information and if it doesn't exists then it should return 404.
    'wareblocks getting a project that does not exist': function (test) {
        'use strict';
        
        // get a random project that does not exist
        var message = mockMessage.projects.get('2');
        
		
        mock.request(message, mock.wareblocksCredentials)
			.then(function (result) {
				test.equal(result.response.statusCode, 404, 'Status code must be 401.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
    },
    // wareblocks application should be able to get a project information 
    'wareblocks getting a project that exists': function (test) {
        'use strict';
        
        var message = mockMessage.projects.get('1');
        
        mock.request(message, mock.wareblocksCredentials)
            .then(function (result) {
                test.equal(result.response.statusCode, 200, 'Status code must be 200.');
				test.equal(result.body.key, mock.wareblocksCredentials.key);
            }).fail(function (err) {
                test.equal(err, null, 'There should be no errors');
            }).fin(function () {
                test.done();
            });
    },
   // tests q request of the mock
    'wareblocks getting multiple projects while using qRequest': function (test) {
        'use strict';
        
        // get a random project that does not exist
        var message = mockMessage.projects.get('2');
        
        // tests mock q request 
        mock.request(message, mock.wareblocksCredentials)
            .fail(function (error) {
                test.equal(error, null, 'There should be no error.');
            })
            .then(function (res) {
                test.equal(res.response.statusCode, 404, 'Status code must be 404.');
                return mock.request(message, invalidCredentials);
            })
            .fail(function (error) {
                test.equal(error, null, 'There should be no error.');
            })
            .then(function (res) {
                test.equal(res.response.statusCode, 401, 'Status code must be 404.');
            }).fin(function () {
                test.done();
            });
    },
     // wareblocks should be able to create and remove a project. Status code should be 204 for both.
    'wareblocks creating a project': function (test) {
        'use strict';
        
        var create = mockMessage.projects.create('test', 'test'),
			remove = mockMessage.projects.remove('test');
        
        // tests mock q request 
        mock.request(create, mock.wareblocksCredentials)
            .fail(function (error) {
                test.equal(error, null, 'There should be no error.');
            })
            .then(function (res) {
                test.equal(res.response.statusCode, 204, 'Status code must be 204.');
				// remove after creation.
                return mock.request(remove, mock.wareblocksCredentials);
            })
			.fail(function (error) {
                test.equal(error, null, 'There should be no error.');
            })
            .then(function (res) {
                test.equal(res.response.statusCode, 204, 'Status code must be 204.');
            })
			.fin(function () {
				test.done();
			});
    }
};
