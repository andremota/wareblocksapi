/*global module: true*/

var http = require('http'),
    Hmmac = require('hmmac'),
    hmmac = new Hmmac();

var mock = {};

mock.options = {
    port: 3000,
    host: 'localhost'
};

mock.wareblocksCredentials = {key: '1', secret: '1'};

// Executes a request to host:port configured in the mock.options
mock.request = function (request, credentials, success) {
    'use strict';
    
    var req;
    
    if (request.body && typeof (request.body) !== 'string') {
        request.body = JSON.stringify(request.body);
    } else if (!request.body) {
        request.body = '';
    }
    
    if (typeof (credentials) === 'function') {
        success = credentials;
    } else if (credentials) {
        hmmac.sign(request, credentials);
    }
    
    req = http.request(request, success);
    req.write(request.body);
    req.end();
};

// Creates a mock message
mock.message = function (path, method, payload) {
    'use strict';
    return {
        host: mock.options.host,
        port: mock.options.port,
        path: path,
        method: method,
        body: payload,
        headers: {
            'Content-Type': 'application/json',
            'Date': new Date().toUTCString()
        }
    };
};


module.exports = mock;