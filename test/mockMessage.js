
/*global module: true*/

var mock = {};

// Project Mock Messages
mock.projects = {
    // Create project
    create: function (id, secret) {
        'use strict';
        return mock.strategy.message('/project', 'POST', {id: id, key: secret});
    },
	// remove project
    remove: function (id) {
        'use strict';
        return mock.strategy.message('/project/' + id, 'DELETE');
    },
    // Get project info
    get: function (id) {
        'use strict';
        return mock.strategy.message('/project/' + id, 'GET');
    }
};

mock.features = {
	// creates a feature for a given project
	createOrUpdate: function (projectId, featureId, tokens) {
		'use strict';
		return mock.strategy.message('/project/' + projectId + '/feature/' + featureId, 'PUT', {tokens: tokens || []});
	},
	// creates a feature for a given project
	get: function (projectId, featureId) {
		'use strict';
		return mock.strategy.message('/project/' + projectId + '/feature/' + featureId, 'GET');
	},
	// removes a feature for a given project
	remove: function (projectId, featureId) {
		'use strict';
		return mock.strategy.message('/project/' + projectId + '/feature/' + featureId, 'DELETE');
	}
};

module.exports = function (mockStrategy) {
    'use strict';
    mock.strategy = mockStrategy;
    return mock;
};