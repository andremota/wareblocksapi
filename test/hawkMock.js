/*global module: true*/

var q = require('q'),
    hawk = require('hawk'),
    crypto = require('crypto'),
    http = require('http');

var mock = {};

var nonce = function (cb) {
    'use strict';
    crypto.randomBytes(16, function (ex, buf) {
        var token = buf.toString('hex');
        cb(token);
    });
};

var qNonce = function () {
    'use strict';
    return q.Promise(function (resolve) {
        crypto.randomBytes(16, function (ex, buf) {
            var token = buf.toString('hex');
            resolve(token);
        });
    });
};

// Mock client options
mock.options = {
    scheme: 'http',
    port: 3000,
    host: 'localhost'
};

// Generate crendentials
mock.credentials = function (id, key) {
    'use strict';
    return {id: id, key: key, algorithm: 'sha256'};
};

// Valid Credentials
mock.wareblocksCredentials = mock.credentials('1', '2');

mock.request = function (options, credentials) {
    'use strict';
    return q.Promise(function (resolve, reject) {
        qNonce()
            .then(function (nonce) {
                
				var header,
					callback,
					req;
                
                if (credentials) {
					
					console.log('\nPayload: ' + options.payload);
					
                    header = hawk.client.header(options.uri, options.method,
                                            { credentials: credentials, ext: credentials.id,  contentType: 'application/json', nonce: nonce, payload: options.payload });
					
                    options.headers.authorization = header.field;
					
					console.log('\nHeaders: ' + JSON.stringify(options.headers));
                }
			
				callback = function (res) {
					var str = '';
					res.on('data', function (chunk) {
						console.log('\nData: ' + chunk);
						str += chunk;
					});
					res.on('end', function () {
						console.log('\nEnd (Body: ' + str + ')');
						var validResponse = true;
						
						if (header) {
							validResponse = hawk.client.authenticate(res, credentials, header.artifacts, { payload: str, contentType: 'application/json' });
						}
						if (validResponse) {
							resolve({response: res, body: str !== '' ? JSON.parse(str) : {} });
						} else {
							reject(new Error('Could not validate server response authenticity.'));
						}
					});
					req.on('error', function (e) {
						console.log('\nError: ' + e);
						reject(e);
					});
				};
            
                req = http.request(options, callback);
			
				if (options.payload !== '') {
					req.write(options.payload);
				}
			
				req.end();
            });
    });
};

// Creates a mock message
mock.message = function (path, method, payload) {
    'use strict';

	payload = payload ? JSON.stringify(payload) : '';
	
    var message = {
		uri: mock.options.scheme + '://' + mock.options.host + ':' + mock.options.port + path,
		hostname: mock.options.host,
		port: mock.options.port,
		path: path,
        method: method,
        headers: {
            'content-type': 'application/json',
			'content-length': payload.length
        },
		payload: payload
    };
	
	return message;
};

module.exports = mock;