/*global require: true, exports: true */
/*jslint nomen: true*/
var mock = require('./hawkMock'),
    mockMessage = require('./mockMessage.js')(mock),
    invalidCredentials = mock.credentials('1', '5'),
	mongoose = require('mongoose');

exports.project_tests = {
    // test set up
    setUp: function (done) {
        'use strict';
        done();
    },
	/*'should not be possible to create permissions without specifying credentials': function (test) {
        'use strict';
        
        var message = mockMessage.features.createOrUpdate('project', 'somerandomid');
        
        mock.request(message)
			.then(function (result) {
				test.equal(result.response.statusCode, 401, 'Status code must be 401.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
	},
	'should not be possible to create permissions with invalid credentials': function (test) {
        'use strict';
        
        var message = mockMessage.features.createOrUpdate('project', 'somerandomid2');
        
        mock.request(message, invalidCredentials)
			.then(function (result) {
				test.equal(result.response.statusCode, 401, 'Status code must be 401.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
	},
	'should not be possible to get permissions information without specifying credentials': function (test) {
        'use strict';
        
		var id = mongoose.Types.ObjectId().toString(),
			message = mockMessage.features.get('project', id);
        
        mock.request(message)
			.then(function (result) {
				test.equal(result.response.statusCode, 401, 'Status code must be 401.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
	},
	'should not be possible to get permissions information with invalid credentials': function (test) {
        'use strict';
        
		var id = mongoose.Types.ObjectId().toString(),
			message = mockMessage.features.get('project', id);
        
        mock.request(message, invalidCredentials)
			.then(function (result) {
				test.equal(result.response.statusCode, 401, 'Status code must be 401.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
	},
	'wareblocks getting a feature that does not exist': function (test) {
        'use strict';
        
		var id = mongoose.Types.ObjectId().toString(),
			message = mockMessage.features.get('project', id);
        
        mock.request(message, mock.wareblocksCredentials)
			.then(function (result) {
				test.equal(result.response.statusCode, 404, 'Status code must be 404.');
			})
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.fin(function () {
                test.done();
            });
	},*/
	'wareblocks getting a feature that just created and then removes it': function (test) {
        'use strict';
        
        var create = mockMessage.features.createOrUpdate('project', 'somerandomid3'),
			get = mockMessage.features.get('project', 'somerandomid3'),
			remove = mockMessage.features.remove('project', 'somerandomid3'),
			createdResponse;
        
        mock.request(create, mock.wareblocksCredentials)
			// CREATE
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.then(function (result) {
				createdResponse = result.body;
			
				test.equal(result.response.statusCode, 204, 'Status code must be 204');
			
				return mock.request(get, mock.wareblocksCredentials);
			})
			// GET
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.then(function (result) {
			
				test.equal(result.response.statusCode, 200, 'Status code must be 200.');
			
				test.equal(result.body.projectId, 'project', 'Project id must be "project"');
				test.equal(result.body.featureId, 'somerandomid3', 'Feature id must be "somerandomid3"');
			
				return mock.request(remove, mock.wareblocksCredentials);
			})
			// REMOVE
			.fail(function (err) {
				test.equal(err, null, 'There should be no errors');
			})
			.then(function (result) {
				test.equal(result.response.statusCode, 204, 'Status code must be 204.');
			})
			.fin(function () {
                test.done();
            });
	}
		
};