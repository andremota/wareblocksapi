/*global require: true, exports: true */

var restify = require('restify');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

exports.restify_server_tests = {
    setUp: function (done) {
        'use strict';
        // setup here
        done();
    },
    'no args': function (test) {
        'use strict';
        
        var server = restify.createServer({ name: 'testServer' }),
            serverEqual = restify.createServer();
        
        test.notStrictEqual(server, serverEqual, 'both servers should reference the same server object');
        
        test.done();
    }
};
