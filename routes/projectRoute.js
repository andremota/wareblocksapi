/*global module:true, require: true */

var express = require('express'),
	auth = require('../middlewares/hawk'),
    common = require('../middlewares/common'),
    project = require('../middlewares/project');

var router = express.Router();

// create a project
router.post('/project/',
                auth, common.wareblock, project.addOrUpdate, common.errorResponse, common.noContentResponse);
    
// removes a project
router['delete']('/project/:projectId',
                auth, common.wareblock, project.remove, common.errorResponse, common.noContentResponse);

// get Project info
router.get('/project/:projectId',
               auth, common.wareblock, project.get, common.errorResponse, common.modelResponse);

module.exports = router;