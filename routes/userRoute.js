/*global module:true, require: true */

var express = require('express'),
	auth = require('../middlewares/hawk'),
    common = require('../middlewares/common'),
    user = require('../middlewares/user');

var router = express.Router();

// get user permissions
router.get('/project/me/user/:userId/permissions',
               auth, user.getPermissions, common.errorResponse, common.modelResponse);
    
// add user permissions
router.post('/project/me/user/:userId/permissions',
                auth, user.addPermissions, common.errorResponse, common.noContentResponse);

// add user permissions
router.post('/project/:projectId/user/:userId/permissions',
                auth, common.wareblock, user.addPermissions, common.errorResponse, common.noContentResponse);


module.exports = router;