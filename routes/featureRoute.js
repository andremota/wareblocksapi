/*global module: true, require: true*/

var express = require('express'),
	auth = require('../middlewares/hawk'),
    common = require('../middlewares/common'),
    feature = require('../middlewares/feature');

var router = express.Router();

// create a new feature
//router.post('/project/:projectId/feature/',
//			auth, common.wareblock, feature.add, common.errorResponse, common.modelResponse);

// update feature
router.put('/project/:projectId/feature/:featureId',
			auth, common.wareblock, feature.addOrUpdate, common.errorResponse, common.noContentResponse);

// get feature info
router.get('/project/:projectId/feature/:featureId',
			auth, common.wareblock, feature.get, common.errorResponse, common.modelResponse);

// get feature info
router['delete']('/project/:projectId/feature/:featureId',
			auth, common.wareblock, feature.remove, common.errorResponse, common.noContentResponse);

module.exports = router;
