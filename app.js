/*jslint nomen: true*/
/*global require: true, __dirname: true, console: true */

var path = require('path'),
    nconf = require('nconf'),
    mongoose = require('mongoose');

// loads app.config.json and env variables to nconf object.
require('./config/nconf.conf.js')(__dirname, 'app.config.json');

// connects to mongodb and loads entities.
require('./config/mongoose.conf.js')(path.join(__dirname, '/entities'));

// configures restify server and loads routes.
var server = require('./config/express.conf.js')(path.join(__dirname, '/routes'));

// start listening on 'api_port' which is configured on the app.config.json file.
server.listen(nconf.get('api_port'), function () {
    'use strict';
    console.log('%s listening at %s', server.name, server.url);
});

if (nconf.get('wareblocks_assure_creation')) {
    // create wareblocks project
    var project = mongoose.model('Project');
    project.addOrUpdate(nconf.get('wareblocks_app_id'), nconf.get('wareblocks_app_key'))
        .then(function (model) {
            'use strict';
            console.log('Created: ' + JSON.stringify(model));
        }).fail(function (err) {
            'use strict';
            console.log(JSON.stringify(err));
        });
}