/*global module: true, require: true */

var q = require('q'),
    mongoose = require('mongoose');

var schema = mongoose.Schema;

//  Project Entity

module.exports = function () {
    
    'use strict';
    
    var userSchema = schema({
        id: String,
        projectId: String,
        permissions: [ {type: String } ]
    });

    userSchema.statics.addOrUpdate = function (projectId, userId, permissions) {
        var deferred = q.defer();
        this.update({id: userId, projectId: projectId}, {$push: {permissions: permissions}}, {upsert: true}, deferred.makeNodeResolver());
        return deferred.promise;
    };
    
    userSchema.statics.get = function (projectId, userId) {
        var deferred = q.defer();
        this.findOne({id: userId, projectId: projectId}, deferred.makeNodeResolver());
        return deferred.promise;
    };
    
    return userSchema;
};
