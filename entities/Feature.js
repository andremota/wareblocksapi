/*global module: true, require: true */
/*jslint nomen: true*/

var q = require('q'),
    mongoose = require('mongoose');

var schema = mongoose.Schema;

module.exports = function () {
    
    'use strict';
    
    var featureSchema = schema({
		featureId: String,
        projectId: String,
        tokens: [ {type: String} ]
    });
	
	featureSchema.index({projectId: 1, featureId: 1}, {unique: true});

    featureSchema.statics.add = function (projectId, permission, tokens) {
        var deferred = q.defer();
        this.findOneAndUpdate({projectId: projectId, permission: permission}, {$addToSet: {tokens: tokens}}, {upsert: true}, deferred.makeNodeResolver());
        return deferred.promise;
    };
	
    featureSchema.statics.get = function (projectId, featureId) {
        var deferred = q.defer();
        this.findOne({projectId: projectId, featureId: featureId}, deferred.makeNodeResolver());
        return deferred.promise;
    };
    
    featureSchema.statics.addOrUpdate = function (projectId, featureId, tokens) {
        var deferred = q.defer();
        this.findOneAndUpdate({projectId: projectId, featureId: featureId}, {$addToSet: {tokens: tokens || []}}, {upsert: true}, deferred.makeNodeResolver());
        return deferred.promise;
    };
	
	featureSchema.statics.remove = function (projectId, featureId) {
        var deferred = q.defer();
		this.findOneAndRemove({projectId: projectId, featureId: featureId}, deferred.makeNodeResolver());
        return deferred.promise;
    };
    
    return featureSchema;
};
