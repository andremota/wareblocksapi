/*global module: true, require: true */

var q = require('q'),
    mongoose = require('mongoose');

var schema = mongoose.Schema;

//  Project Entity

module.exports = function () {
    
    'use strict';
    
    var projectSchema = schema({
        id: {
            type: String,
            require: true
        },
        key: {
            type: String,
            required: true
        }
    });
    
    projectSchema.statics.add = function (id, key) {
        var deferred = q.defer();
        this.create({id: id, key: key}, deferred.makeNodeResolver());
        return deferred.promise;
    };
    
    projectSchema.statics.get = function (id) {
        var deferred = q.defer();
        this.where({id: id}).findOne(deferred.makeNodeResolver());
        return deferred.promise;
    };
    
    projectSchema.statics.addOrUpdate = function (id, key) {
        var deferred = q.defer();
        this.findOneAndUpdate({id: id}, {$set: {key: key }}, {upsert: true}, deferred.makeNodeResolver());
        return deferred.promise;
    };
	
	projectSchema.statics.remove = function (id) {
		var deferred = q.defer();
		this.findOneAndRemove({id: id}, deferred.makeNodeResolver());
		return deferred.promise;
	};

    return projectSchema;
};
