# wareblocksapi

Wareblocks Node.js API

___

## Getting Started

Install node >= 0.8.0

Install MongoDb

Install the modules with: `npm install`

Install grunt-cli: `npm install -g grunt-cli`

___

## Api Documentation

### Projects

#### Create Project (WareBlock)

Only wareblock can use this endpoint.

Endpoint: `POST /project/`

Body:
```json
{
    "project_id": 134554DSfDRSDARJKCHSA1343254,
    "project_secret": 14kj32dal123214qADASd3242342354efASDFSf
}
```

Example Response:
STATUS CODE: 204

#### Get Project Info (WareBlock)

Only wareblock can use this endpoint.

Endpoint: `GET /project/{projectId}`

Example Response:
STATUS CODE: 200
```json
{
    "project_id": 134554DSfDRSDARJKCHSA1343254,
    "project_secret": 14kj32dal123214qADASd3242342354efASDFSf
}
```

### Features

#### Create Feature (WareBlock)

Only wareblock can use this endpoint.

Endpoint: `POST /project/{projectId}/feature/`

Body:
```json
{
    "permission": "/test/permission"
}
```

Example Response:
STATUS CODE: 204

#### Add tokens to Feature

Only wareblock can use this endpoint.

Endpoint: `POST /project/{projectId}/feature/tokens`

Body:
```json
{
    "permission": "/test/permission",
    "tokens": ["token1", "token2"]
}
```

Example Response:
STATUS CODE: 204


### Users

### Get User Permissions

Endpoint: `GET /project/{projectId}/user/{userId}/permissions`

Example Response:

STATUS CODE: 200
```json
{
    "user_id": "awesome@wareblocks.com",
    "project_id": 134554DSfDRSDARJKCHSA1343254,
    "permissions": ["/test/permission"]
}
```

### Add User Permission

Endpoint: `POST /project/{projectId}/user/{userId}/permissions`

Body:
```json
{
    "permissions": ["/test/permission"]
}
```

Example Response:

STATUS CODE: 204

___

## Dev Documentation

### Starting the api

Run `node app` or `npm start`.

### Unit Testing

Create the unit tests under the test folder which is present on the root of the project.
The test suites must finalize with "_test.js" so that they can automatically be run with grunt.

To run the unit tests do the following command `grunt nodeunit` or `npm test` on the project root.

### jshint

To run jshint do the following command: `grunt jshint`

### Grunt

To run both jshint and nodeunit run `grunt` on the project root.

### Global configuration

The configuration is present in the app.config.json. It can also be passed as env variables.

### Routes

The routes must be present in the routes folder which is in the root of the project.
It also must export a function which receives a server object (http://mcavage.me/node-restify/#server-api-2).

### Mongoose Config

Needs "mongo_connection" config to be present to connect to MongoDb.
Loads all the entities present in the entities folder of the project.

### Starting MongoDb

`%MONGODB_HOME%\bin\mongod.exe --dbpath <path to store the db>`

Ex:
`"C:\Program Files\MongoDB 2.6 Standard\bin\mongod.exe" --dbpath "C:\MongoDb\data`

___

## Examples

### Route example:
```javascript

var hmac = require('../middlewares/hmac');

module.exports = function (server) {
    
    'use strict';
    
    var respond = function (req, res, next) {
        res.send('hello world');
        next();
    };
    
    // auth
    server.get('/project/mockWithAuth', hmac, respond);    
    
    // no hmac auth
    server.get('/project/mock', respond);
    
};

```
Create the routes under the 'routes' folder of the project.
A route must receive a server. 
For more information about the server api see: http://mcavage.me/node-restify/#server-api-2

### Configs example:

#### app.config.json file
```javascript
{
    "mongo_connection": "mongodb://localhost",
    "api_name": "wareblocksapi",
    "api_port": 3000
}
```
A file to store global configurations which is in the root of the project.

#### Obtain a config on the app.config.json file:
```javascript
var nconf = require('nconf');

var apiPort = nconf.get('api_port');
```
For the example above, a key api_port must be present in the app.config.json with a value.

To obtain the configuration (key/value) present in the app.config.json on the project root use nconf module as above.

### Unit testing example:
```javascript
exports.restify_server_tests = {
    setUp: function (done) {
        'use strict';
        // setup here
        done();
    },
    'no args': function (test) {
        'use strict';
        
        test.notStrictEqual('a', 'b', 'a is different from b');
        
        test.done();
    }
};
```

___

## Release History
_(Nothing yet)_

___

## License
Copyright (c) 2015 www.wareblocks.com 