/*global require: true, module: true*/

var bodyParser = require('body-parser');

module.exports = function (routesPath) {
    
    'use strict';
    
    var express = require('express'),
        nconf = require('nconf'),
        app = express(),
        routes = require('require-all')(routesPath),
        key;
    
    app.use(bodyParser.json());
    
    for (key in routes) {
        if (routes.hasOwnProperty(key)) {
            app.use(routes[key]);
        }
    }
    
    return app;
};
