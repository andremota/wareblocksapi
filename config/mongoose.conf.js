/*global module:true, require: true */

var nconf = require('nconf');
var mongoose = require('mongoose');


module.exports = function (entitiesPath) {

    'use strict';
    
    var connString = nconf.get('mongo_connection'),
        models = require('require-all')(entitiesPath),
        key,
        model;
    
    // connect to mongodb
    mongoose.connect(connString);
    
    // loads Model definition.
    for (key in models) {
        if (models.hasOwnProperty(key)) {
            model = models[key]();
            mongoose.model(key, model);
        }
    }
    
};
