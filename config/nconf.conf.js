/*global require: true, module: true */

var nconf = require('nconf');

module.exports = function (dir, fileName) {
    'use strict';
    
    nconf.argv()
        .env()
        .add('config',{
			type: 'file',
            dir: dir,
            file: fileName,
            search: true
        });
    
    console.log("name:" + nconf.get('api_name'));
};
